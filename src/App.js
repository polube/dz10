import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { setUsers, setLoading } from "./redux/actions";
import { Container } from "@material-ui/core";
import SelectUsers from "./components/SelectUsers";
import './App.css';
import { API_USERS_URI } from "./constants.json";

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setLoading("almost"))
    fetch(API_USERS_URI)
        .then((response) => response.json())
        .then((data) => dispatch(setUsers(data)));
  }, [dispatch]);

  return (
    <Container>
      <SelectUsers />
    </Container>
  );
}

export default App;
